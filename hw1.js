var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
  
var sketch = document.getElementById('sketch');
var sketch_style = getComputedStyle(sketch);
canvas.width = 800;
canvas.height = 600;
 
let drawing = false;
let pathsry = [];
let points = [];

var mouse = {x: 0, y: 0};
var previous = {x: 0, y: 0};
var mousedown = false;
var currentMethod = "none";
var brush_shape = "round";
var fix_x;
var fix_y;

var undo_buffer = [];
var redo_buffer = [];

var reset, undo, redo, download;

 /* Mouse Capturing Work 
 canvas.addEventListener('mousemove', function(e) {
   mouse.x = e.pageX - this.offsetLeft;
   mouse.y = e.pageY - this.offsetTop;
 }, false);
 *////
 canvas.addEventListener('mousemove', function(e) {
  if(drawing){
  previous = {x:mouse.x,y:mouse.y};
  mouse = oMousePos(canvas, e);
  // saving the points in the points array
  points.push({x:mouse.x,y:mouse.y})
  // drawing a line from the previous point to the current point
  ctx.beginPath();
  ctx.moveTo(previous.x,previous.y);
  ctx.lineTo(mouse.x,mouse.y);
  ctx.stroke();
  }
  }, false);
  
 /* Drawing on Paint App */
 ctx.lineJoin = 'round';
 ctx.lineCap = 'round';
 
 ctx.strokeStyle = "red";
 function getColor(color){ctx.strokeStyle = color;}
 
 function getSize(size){ctx.lineWidth = size;}
 
 
 //ctx.strokeStyle = 
 //ctx.strokeStyle = document.settings.colour[1].value;
 /* 
 canvas.addEventListener('mousedown', function(e) {
     ctx.beginPath();
     ctx.moveTo(mouse.x, mouse.y);
  
     canvas.addEventListener('mousemove', onPaint, false);
 }, false);
 *////
 canvas.addEventListener('mousedown', function(e) {
  drawing = true; 
  previous = {x:mouse.x,y:mouse.y};
  mouse = oMousePos(canvas, e);
  points = [];
  points.push({x:mouse.x,y:mouse.y})
  });
 /*
 canvas.addEventListener('mouseup', function() {
     canvas.removeEventListener('mousemove', onPaint, false);
 }, false);
  *////
  canvas.addEventListener('mouseup', function() {
    drawing=false;
    // Adding the path to the array or the paths
    pathsry.push(points);
    }, false);
 
    undo.addEventListener("click",Undo);
/*    
 var onPaint = function() {
     ctx.lineTo(mouse.x, mouse.y);
     ctx.stroke();
 };
*////
function drawPaths(){
  // delete everything
  ctx.clearRect(0,0,canvas.width,canvas.height);
  // draw all the paths in the paths array
  pathsry.forEach(path=>{
  ctx.beginPath();
  ctx.moveTo(path[0].x,path[0].y);  
  for(let i = 1; i < path.length; i++){
    ctx.lineTo(path[i].x,path[i].y); 
  }
    ctx.stroke();
  })
}  

function Undo(){
  // remove the last path from the paths array
  pathsry.splice(-1,1);
  // draw all the paths in the paths array
  drawPaths();
}
function redo(){
  // remove the last path from the paths array
  pathsry.splice(1,-1);
  // draw all the paths in the paths array
  drawPaths();
}

/*
canvas.on('object:added', function() {
  if (!isRedoing) {
    h = [];
  }
  isRedoing = false;
});

var isRedoing = false;
var h = [];

function undo() {
  if (canvas._objects.length > 0) {
    h.push(canvas._objects.pop());
    canvas.renderAll();
  }
}

function redo() {

  if (h.length > 0) {
    isRedoing = true;
    canvas.add(h.pop());
  }
}
*/



// a function to detect the mouse position
function oMousePos(canvas, evt) {
  var ClientRect = canvas.getBoundingClientRect();
	return { //objeto
	x: Math.round(evt.clientX - ClientRect.left),
	y: Math.round(evt.clientY - ClientRect.top)
}
}


/*function erase(size){
  ctx.lineWidth = size;
  ctx.globalCompositeOperation="destination-out";
  ctx.arc(currentX,currentY,8,0,Math.PI*2,false);
}

function clean(){
  ctx.gclearRect(0,0,myCanvas.width,myCanvas.height);
}

function download(){
  var _url=canvas.toDataURL();
  canvas.herf=_url;
}
*/
//function erase(){
//  document.getElementsByTagName("body")[0].style.cursor = "not-allowed";
//}


function erase(size){
  //ctx.ec();
  document.getElementsByTagName("canvas")[0].style.cursor = "url('era.jpg'), auto";
  ctx.lineWidth = size;
  ctx.globalCompositeOperation="destination-out";
  ctx.arc(currentX,currentY,8,0,Math.PI*2,false);
}


function brush(size){
  //ctx.bc();
  document.getElementsByTagName("canvas")[0].style.cursor = "url('pen2.jpg'), auto";
  ctx.lineWidth = size;
  ctx.globalCompositeOperation="source-over";
  ctx.arc(currentX,currentY,8,0,Math.PI*2,false);
  //document.getElementsByTagName("canvas")[0].style.cursor = "url('p.jpg'), auto";
  //document.getElementsByTagName("body")[0].style.cursor = "url('draw.jpg'), auto";
}
//function brush(){
//  document.getElementsByTagName("body")[0].style.cursor = "url('draw.jpg'), auto";
//}

function clean(){
  ctx.clearRect(0,0,canvas.width,canvas.height);
}

//function undo() {
//  if(undo_buffer.length>0){
//    redo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
//    ctx.putImageData(undo_buffer.pop(), 0, 0);
//  }
//}      

//function redo() {
//  if(redo_buffer.length>0){
//    undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
//    ctx.putImageData(redo_buffer.pop(), 0, 0);
//  }
//} 

function cri(){
  document.getElementsByTagName("canvas")[0].style.cursor = "url('1.jpg', auto)";
}
function tri(){
  document.getElementsByTagName("canvas")[0].style.cursor = "url('tri.jpg'), auto";
}
function rec(){
  document.getElementsByTagName("canvas")[0].style.cursor = "url('6.jpg'), auto";
  var tmp;
  ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
  undo_buffer.push(tmp);
  ctx.beginPath();
  ctx.rect(fix_x, fix_y, e.offsetX-fix_x, e.offsetY-fix_y);
  ctx.stroke();
}
function text(){
  document.getElementsByTagName("canvas")[0].style.cursor = "url('t1.png'), auto";
}
// ctx = document.getElementById('myCanvas').getContext("2d");
/*
var step = 0;
function undo() {
  if(step > 0){
    step--
    ctx.clearRect(0,0,canvas.width,canvas.height); //清空画布
    var img = new Image()
    img.src = imgList[step]
    img.onload= function(){ctx.drawImage(img,0,0)}//从数组中调取历史记录，进行重绘
  }else{
    alert('no pre')
  }
}      

function redo() {
  if(step<imgList.length-1){
    step++
    ctx.clearRect(0,0,canvas.width,canvas.height) //清空画布
    var img = new Image()
    img.src = imgList[step]
    img.onload = function(){ctx.drawImage(img,0,0)}//重绘
  }else{
    alert('no next')
  }
} 

window.onload = function() {     
  $('.undo').click(function(){
    
    if(step >= 0){
      step--
      clearCanvas() //清空画布
      var img = new Image()
      img.src = imgList[step]
      img.onload= function(){ctx.drawImage(img,0,0)}//从数组中调取历史记录，进行重绘
    }else{
      alert('没有上一步了')
    }
  })
  $('.redo').click(function(){
    if(step<imgList.length-1){
      step++
      clearCanvas() //清空画布
      var img = new Image()
      img.src = imgList[step]
      img.onload = function(){ctx.drawImage(img,0,0)}//重绘
    }else{
      alert('没有下一步了')
    }
  })
}
*/
/*function undo() {
  if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}      

function redo() {
  if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}  */



  let isMouseDown = false;
  function onMouseDown(e){
    let currentX = e.clientX - this.target.offsetLeft;
    let currentY = e.clientY - this.target.offsetTop;
    if(currentY > this.pickerCircle.y && currentY < this.pickerCircle.y + this.pickerCircle.width && currentX > this.pickerCircle.x && currentX < this.pickerCircle.x + this.pickerCircle.width) {
      isMouseDown = true;
    } else {
      this.pickerCircle.x = currentX;
      this.pickerCircle.y = currentY;
    }
  }
  
  function onMouseMove(e) {
    if(isMouseDown) {
     let currentX = e.clientX - this.target.offsetLeft;
     let currentY = e.clientY - this.target.offsetTop;
      this.pickerCircle.x = currentX;
      this.pickerCircle.y = currentY;
    }
  }
  
  function onMouseUp () {
    isMouseDown = false;
  }
  
  //Register 
  this.target.addEventListener("mousedown", onMouseDown);
  this.target.addEventListener("mousemove", onMouseMove);
  this.target.addEventListener("mousemove", () => this.onChangeCallback(this.getPickedColor()));

  
  document.addEventListener("mouseup", onMouseUp);

/*let eraseEnable = false;
function erase() {
  if (eraseEnable) {
    noErase();
    eraseEnable = false;
  }
  else {
    erase();
    eraseEnable = true;
  }
}
/*function listenForEvents() {
  let isMouseDown = false;
  const onmousedown = (e) => {
    let currentX = e.clientX - this.target.offsetLeft;
    let currentY = e.clientY - this.target.offsetTop;
    if(currentY > this.pickerCircle.y && currentY < this.pickerCircle.y + this.pickerCircle.width && currentX > this.pickerCircle.x && currentX < this.pickerCircle.x + this.pickerCircle.width) {
      isMouseDown = true;
    } else {
      this.pickerCircle.x = currentX;
      this.pickerCircle.y = currentY;
    }
  }
  const onmousemove=(e)=>{
    if(isMouseDown) {
     let currentX = e.clientX - this.target.offsetLeft;
     let currentY = e.clientY - this.target.offsetTop;
      this.pickerCircle.x = currentX;
      this.pickerCircle.y = currentY;
    }
  }
  const onmouseup=()=>{
    isMouseDown = false;
  }
}
*/
